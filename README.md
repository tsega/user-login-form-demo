# User Login Form

A demo Vue CLI generated application to serve as a companion to the "Thinking in Components with Vue.js" course.

## Branches

There are two branches in this repository, one is the `master` branch that has the initial code for the User Login Form, the other is the `final` branch that contains the refactor code.

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
